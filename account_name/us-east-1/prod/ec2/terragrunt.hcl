include {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_terragrunt_dir()}/../../../../modules//ec2"
}

dependency "vpc" {
  config_path = "../vpc"
}

inputs = {
  # Provider
  region            = "us-east-1"
  accountId         = "123707704970"
  avail_zone        = "us-east-1a"

  # EC2
    ec2_instance_type = "t2.micro"
    subnet_id         = dependency.vpc.outputs.vpc_subnet_id_1
    ec2_sg_id         = dependency.vpc.outputs.ec2_sg_id
    ec2_key_pair_name = "devopsbootcamp"
    public_key_location = "/Users/pedrosalzedo/.ssh/devopsbootcamp.pub"
    
}
