include {
    path = find_in_parent_folders()
}

terraform {
    source = "${get_terragrunt_dir()}/../../../../modules//vpc"
}

inputs = {
# Provider
  region            = "us-east-1"
  accountId         = "123707704970"
  avail_zone        = "us-east-1a"

# VPC    
    vpc_cidr_block      = "10.0.0.0/16"
    subnet_cidr_block   = "10.0.10.0/24"
    vpc_sg_name         = "myapp-sg"
}