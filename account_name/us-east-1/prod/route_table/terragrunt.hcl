include {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_terragrunt_dir()}/../../../../modules//route_table"
}

dependency "vpc" {
  config_path = "../vpc"
}

inputs = {
  # Provider
  region            = "us-east-1"
  accountId         = "123707704970"
  avail_zone        = "us-east-1a"

  # Route Table    
  avail_zone      = "us-east-1a"
  vpc_id          = dependency.vpc.outputs.vpc_id
  vpc_subnet_id_1 = dependency.vpc.outputs.vpc_subnet_id_1
}