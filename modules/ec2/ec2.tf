data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_key_pair" "myapp-key" {
  key_name = var.ec2_key_pair_name
}

# output "aws_ami_id" {
#   value = data.aws_key_pair.myapp-key
# }

output "ec2_public_ip" {
  value = aws_instance.myapp-server.public_ip
}

# Create ssh key pair
# resource "aws_key_pair" "ssh-key" {
#   key_name   = var.ec2_key_pair_name
#   public_key = file(var.public_key_location)
# }
###

# EC2 Original without docker script
# resource "aws_instance" "myapp-server" {
#   ami                    = data.aws_ami.latest-amazon-linux-image.id
#   instance_type          = var.ec2_instance_type
#   subnet_id              = var.subnet_id
#   vpc_security_group_ids = ["${var.ec2_sg_id}"]
#   availability_zone      = var.avail_zone

#   associate_public_ip_address = true
#   key_name                    = data.aws_key_pair.myapp-key.key_name

#   tags = {
#     Name = "${var.environment}-server"
#   }
# }

# EC2 with docker
resource "aws_instance" "myapp-server" {
  ami                    = data.aws_ami.latest-amazon-linux-image.id
  instance_type          = var.ec2_instance_type
  subnet_id              = var.subnet_id
  vpc_security_group_ids = ["${var.ec2_sg_id}"]
  availability_zone      = var.avail_zone

  associate_public_ip_address = true
  key_name                    = data.aws_key_pair.myapp-key.key_name

  user_data = file("entry-script.sh")

  tags = {
    Name = "${var.environment}-server"
  }
}