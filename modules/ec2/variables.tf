variable "region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "accountId" {
  type        = string
  description = "description"
}

variable "environment" {
  type        = string
  description = "environment"
}

variable "avail_zone" {
  type        = string
  description = "availalability zone"
}

# EC2
variable "ec2_instance_type" {
  type        = string
  description = "ec2 instance type"
}

variable "subnet_id" {
  type        = string
  description = "subnet id from vpc for ec2 instances"
}

variable "ec2_sg_id" {
  type        = string
  description = "security group id from vpc for ec2 instances"
}

variable "ec2_key_pair_name" {
  type        = string
  description = "key pair ssh"
}

variable "public_key_location" {
  type        = string
  description = "location of public ssh key"

}
