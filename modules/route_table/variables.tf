variable "region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "accountId" {
  type        = string
  description = "description"
}

variable "environment" {
  type        = string
  description = "environment"
}

variable "avail_zone" {
  type        = string
  description = "availalability zone"
}

# rt
variable "vpc_id" {
  type        = string
  description = "vpc id from vpc module"
}

variable "vpc_subnet_id_1" {
  type        = string
  description = "subnet id from vpc subnet 1"
}

