resource "aws_route_table" "myapp-route_table" {
    vpc_id = var.vpc_id
    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.myapp-igw.id
    }
    tags = {
      "Name" = "${var.environment}-route_table"
    }
}

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = var.vpc_id
  tags = {
    "Name" = "${var.environment}-igw"
  }
}

resource "aws_route_table_association" "a-trb-subnet" {
  subnet_id = var.vpc_subnet_id_1
  route_table_id = aws_route_table.myapp-route_table.id
}