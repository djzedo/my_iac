output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.myapp-vpc.id
}

output "vpc_subnet_id_1" {
  description = "subnet association with route tables"
  value       = aws_subnet.myapp-sunbnet-1.id
}

output "ec2_sg_id" {
  description = "security group of the vpc"
  value       = aws_security_group.myapp-sg.id
}