variable "region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "accountId" {
  type        = string
  description = "description"
}

variable "environment" {
  type        = string
  description = "environment"
}

variable "avail_zone" {
  type        = string
  description = "availalability zone"
}

# VPC
variable "vpc_cidr_block" {
  type        = string
  description = "Description"
}

variable "subnet_cidr_block" {
  type        = string
  description = "description"
}

variable "vpc_sg_name" {
  type        = string
  description = "security group name for vpc"
}

